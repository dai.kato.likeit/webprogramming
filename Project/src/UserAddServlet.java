

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//URLで画面に飛ばすことをできないようにする処理
		HttpSession session = request.getSession();
		User u =  (User)session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		//useradd.jspにフォワードで飛ばす
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
     		String loginId = request.getParameter("loginId");
     		String password = request.getParameter("password");
     		String repassword = request.getParameter("repassword");
     		String name = request.getParameter("name");
     		String birthDate = request.getParameter("birthDate");
     	// パスワードの確認の判定


   	    if(!password.equals(repassword)) {
    	    	// リクエストスコープにエラーメッセージをセット
    			request.setAttribute("errMsg", "登録に失敗しました");
    	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
    			dispatcher.forward(request, response);
    			return;    	    }

   	    // ログインIDの重複の判定
   	    User cofirmid = UserDao.findByLoginId(loginId);
   	    if(cofirmid != null) {
   	        // リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "登録されたIDはすでに存在します");
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
			dispatcher.forward(request, response);
			return;
   	    }

   	    // 未入力の項目がある場合のエラー処理
   	    if(loginId.equals("")||password.equals("")||repassword.equals("")||name.equals("")||birthDate.equals("")) {
   	    	// リクエストスコープにエラーメッセージをセット
   	  			request.setAttribute("errMsg", "未入力項目があります");
   	  	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
   	  			dispatcher.forward(request, response);
   	  			return;
   	    }



     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    		UserDao userDao = new UserDao();
    	    userDao.insertInfo(loginId,password,name,birthDate);
    	// リダイレクト
    	    response.sendRedirect("UserListServlet");






	}



}
