package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	/**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
	public User findByLoginInfo(String loginId,String password) {
		Connection conn = null;
		try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            String pass = encryption(password);
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, pass);
            ResultSet rs = pStmt.executeQuery();




            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	/**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
	public static User findByLoginId(String loginId) {
		Connection conn = null;
		try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();




            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }



/**
 * 全てのユーザ情報を取得する
 * @return
 */
public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql = "select * from user where name != 'admin'";

         // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

            userList.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;
}
public void insertInfo(String loginId,String password,String name,String birthDate) {
	Connection conn = null;
	try {
		 // データベースへ接続
        conn = DBManager.getConnection();
        // INSERT文でデータを入れる
        String sql = "INSERT INTO user (login_id, password,name,birth_date,create_date,update_date)VALUES(?,?,?,?,NOW(),NOW())" ;
        //PreparedStatement
        String pass = encryption(password);

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, loginId);
        ps.setString(2, pass);
        ps.setString(3, name);
        ps.setString(4, birthDate);





        ps.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

public User findLoginId(String userId) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を実行
        String sql = "SELECT * FROM user WHERE id = ?";

        //PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, userId);
        ResultSet rs = ps.executeQuery();

        // DBの問題
        // rs.nextで一段下げる作業をする
        if(!(rs.next())) {
        	return null;
        }


        // beansにデータを詰めているので参照する時はbeansを見る
        // DBにアクセスするのはDAOの仕事なのでここでカラム名などを取る
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        String createDate = rs.getString("create_date");
        String updateDate = rs.getString("update_date");
        // インスタンスを生成して戻す
        User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
        // beansのコンストラクタに引数を渡す
        return user;

		}catch (SQLException e) {
			e.printStackTrace();
            return null;
		}	finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
//新規登録画面
public User userupdate(String usersId) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を実行
        String sql = "SELECT * FROM user WHERE id = ?";

        //PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, usersId);
        ResultSet rs = ps.executeQuery();

        // DBの問題
        // rs.nextで一段下げる作業をする
        if(!(rs.next())) {
        	return null;
        }


        // データをbeansに詰める詳細画面で出したい情報だけを詰める
        String loginId = rs.getString("login_id");
        String username = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");

        // インスタンスを生成して戻す
        // beansのコンストラクタに引数を渡す
        return new User(loginId,username,birthDate);

	}catch (SQLException e) {
		e.printStackTrace();
        return null;
	}	finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
//更新画面
public void update(String id,String password,String name,String birthdate) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        // UPDATE文の実行
        String sql = "UPDATE user SET name = ?, birth_date = ?,password=?,update_date=now() where id=?";
        //PreparedStatement
        String pass = encryption(password);
        PreparedStatement ps = conn.prepareStatement(sql);





        ps.setString(1, name);
        ps.setString(2, birthdate);
        ps.setString(3, pass);
        ps.setString(4, id);

        ps.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();

		}	finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
    }
//削除画面
public void delete(String id) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        //DELETE文の実行
        String sql = "DELETE FROM user WHERE id = ?";

        //PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);

        ps.setString(1, id);
        ps.executeUpdate();

	}catch (SQLException e) {
		e.printStackTrace();

	}	finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }
}
//暗号化メソッド
public String encryption(String password) {
	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);



	return result;

}
//検索用のメソッド
public ArrayList<User> search(String loginId,String name,String startDate,String endDate) {
	Connection conn = null;
	ArrayList<User> list = new ArrayList<User>();
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        //SELECT(検索)文の実行
        String sql = "select * from user where name != 'admin'";

        if(!loginId.equals("")) {
        	sql += " and login_id = '" + loginId + "'";
        }

        //部分一致検索
        //sqlを結合している

        if(!name.equals("")) {
        	sql += " and name like '%" + name + "%'";
        }

        //生年月日検索
        if(!startDate.equals("")) {
        	sql += " and  birth_date >= '" + startDate +"'";
        }

        if(!endDate.equals("")) {
        	sql += " and  birth_date <= '" + endDate +"'";
        }


        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);



        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId2 = rs.getString("login_id");
            String name2 = rs.getString("name");
            Date birthDate2 = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId2, name2, birthDate2, password, createDate, updateDate);

            list.add(user);
        }

	}catch (SQLException e) {
		e.printStackTrace();

	}	finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }

	return list;

}

}






