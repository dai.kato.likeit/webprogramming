

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//URLで画面に飛ばさせない為の処理
		HttpSession session = request.getSession();
		User u =  (User)session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
				//ここでuserlist.jspからの情報を受け取る
				// リクエストパラメータの文字コードを指定
		        request.setCharacterEncoding("UTF-8");

		        // パラメータの受け取り
		        String userId = request.getParameter("id");


		        // DAOに情報を詰める
		        UserDao userdao = new UserDao();
		        // ここで戻り値を受け取る
		        User userInfo = userdao.findLoginId(userId);

		        // セッションにユーザの情報をセット
				//HttpSession session = request.getSession();
				session.setAttribute("user",userInfo);

				// フォワード先
				// フォワードさせる時は絶対に最後に書く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Userdetail.jsp");
				dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
