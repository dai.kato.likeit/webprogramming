

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User u =  (User)session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // userList.jspからのパラメータをここで受け取っている
        String usersId = request.getParameter("id");

        // daoのインスタンス生成
        UserDao userdao = new UserDao();

        // ここで戻り値を受け取る
        User userdetail = userdao.findLoginId(usersId);

        // セッションスコープにユーザの情報をセット
		//HttpSession session = request.getSession();
		session.setAttribute("user",userdetail);

		// フォワード先
		// フォワードさせる時は絶対に最後に書く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        //リクエストパラメターを受け取る
      		String id	=	request.getParameter("id");
      		String password	=	request.getParameter("password");
      		String repassword = request.getParameter("repassword");
      		String name = request.getParameter("name");
      		String birthdate = request.getParameter("birthdate");

      		//パスワードの確認の判定
       	    if(!password.equals(repassword)) {
        	// リクエストスコープにエラーメッセージをセット
        	request.setAttribute("errMsg", "登録に失敗しました");
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
        	dispatcher.forward(request, response);
        	return;    	    }

       	    if(password.equals(null)) {
       	    	// リクエストスコープにエラーメッセージをセット
            	request.setAttribute("errMsg", "登録に失敗しました");
            	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
            	dispatcher.forward(request, response);
            	return;
       	    }
       	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    		UserDao userDao = new UserDao();
    	    userDao.update(id,password,name,birthdate);

    	 // リダイレクト
    	    response.sendRedirect("UserListServlet");

	}

}
