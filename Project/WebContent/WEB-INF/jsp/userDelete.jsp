<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ削除確認</title>
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <li class="navbar-text">${userInfo.name} さん </li>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <div align=center>
        <h1>ユーザ削除確認</h1>
        <p>${userdelete.loginId}を本当に削除してよろしいでしょうか?</p>


        <p><a href="UserListServlet"><input type=submit value="いいえ"></a></p>
        <br>
        <!-- formタグの場所に注意!!! formタグで括ったtype submitはそれ以降全部postで作動してしまう -->
		<form method="post" action="UserDeleteServlet">
		<p><input type=submit  value="はい"></p>
		<!-- hiddenパラメターはformタグの下ならどこでも良い -->
    	<input type="hidden" name="id" value="${userdelete.id}">
         </form>
         </div>

         </body>

