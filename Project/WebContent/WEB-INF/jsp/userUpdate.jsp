<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
    <html>

    <head>
        <meta charest=UTF-8>
        <title>ユーザー情報更新</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <div class="container">

	<body>
        <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
            <form method="post" action="UserUpdateServlet">
            	<!-- hiddenパラメターはformタグの下ならどこでも良い -->
            	<input type="hidden" name="id" value="${user.id}">
                <header>
                    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
                        <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
                        </ul>
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                               <li class="navbar-text">${userInfo.name} さん </li>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                            </li>
                        </ul>
                    </nav>

                </header>
                <br>
                <p>ログインID ${user.loginId}</p>
                <p>パスワード<input type="password" name="password" size="30"></p>
                <p>パスワード(確認)<input type="password" name="repassword" size="30"></p>
                <p>ユーザー名<input type="text"  id="userName" name="name" value="${user.name}"> </p>
                <p>生年月日<input type="date"   id="birthDate" name="birthdate"value="${user.birthDate}"></p>

                <br>
                <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>

                <a href="UserListServlet">戻る</a>



            </form>
            </body>

    </div>

    </html>
