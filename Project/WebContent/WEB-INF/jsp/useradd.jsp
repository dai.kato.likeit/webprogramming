<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

    <meta charest=UTF-8>
    <title>ユーザー新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <!-- header -->
    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <li class="navbar-text">${userInfo.name} さん </li>

                <li class="nav-item">
                    <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <!-- /header -->
    <div align=center>
        <h1>ユーザ一新規登録</h1>
        <!-- /ここでUserAddServletのpostメソッドに飛ばす -->
        <form action="UserAddServlet" method="post">

        ログインID <input type="text" name="loginId" size="40" maxlength="30">



        <p></p>

        パスワード<input type="password" name="password" size="40" maxlength="30">


        <p></p>

        パスワード(確認) <input type="password" name="repassword" size="40" maxlength="30">


        <p></p>

        ユーザー名 <input type="text" name="name" size="40" maxlength="30">


        <p></p>
        <!-- /絶対にdate型にする -->
        生年月日 <input type="date" name="birthDate" size="40" maxlength="30">


        <p></p>

        <input type="submit" value="登録">

        <div class="col-xs-4">
            <a href="UserListServlet">戻る</a>
        </div>
	</form>
    </div>
</body>

</html>
