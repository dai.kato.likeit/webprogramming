<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー覧</title>
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
        <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
        </ul>
        <ul class="navbar-nav flex-row">
            <li class="nav-item">
                <li class="navbar-text">${userInfo.name} さん </li>
            <li class="nav-item">
                <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
            </li>
        </ul>
    </nav>
</header>

<div class="container">
<div align=center>
    <br>
		 <form method="post" action="UserListServlet" class="form-horizontal">
        <h1>ユーザ一覧</h1>
        <a href="UserAddServlet">新規登録</a>
        <p>ログインID <input type=text name = "loginId"></p>
        <p>ユーザー名<input type=text name = "name"></p>
        <p>生年月日<input type="date" name = "birthDate"></p>〜<p>生年月日<input type="date" name = "rebirthDate"></p>

        <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
    	</form>
    <table class="table">

        <tr>
            <th>ログインID</th>
            <th>ユーザ名</th>
            <th>生年月日</th>
            <th></th>
        </tr>


<tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <td>
                     	<c:if test="${userInfo.loginId == 'admin' }">
                     	<!-- /このEL式でuser.idのインスタンスを飛ばす -->
                     	<!-- /Servletのdogetメソッドで受け取る -->
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                      	</c:if>

                      	<c:if test="${userInfo.loginId != 'admin' }">
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                      	<c:if test="${userInfo.loginId == user.loginId }">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
						</c:if>
                      	</c:if>



                     </td>


                   </tr>
                 </c:forEach>




    </table>

</div>
</div>